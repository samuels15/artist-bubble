As of March 25th 2020, to get started on this project, you need an developer account on Last.fm. Once you do that, you can get an personal and secret API KEY which you can save on a .txt file named "api_key.txt" and save it in the same directory that the main files in this project.

The file lastfm.py contains functions to retrieve data from the API.
The file data_treatment.py contains functions to manipulate and transform this data to a format that can be used by the following steps of the application (still in development phase)

Useful links:
https://medium.com/@sureshssarda/pandas-splitting-exploding-a-column-into-multiple-rows-b1b1d59ea12e
