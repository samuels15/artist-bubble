import sys
import pandas as pd
from numpy import nan, concatenate, split
from math import ceil
from time import sleep

from neo4j_bubble import db_run_line
from lastfm import searchTracks
from progressBar import progressBarSize

def db_dropall():
    db_run_line("MATCH (n) DETACH DELETE (n)");

def df_chunks(df, chunk_size):
    indices = range(chunk_size, int(ceil(df.shape[0] / chunk_size) * chunk_size), chunk_size)
    return split(df, indices)

def db_create_relationships (rdf):
    pbar = progressBarSize(rdf.shape[0])/10
    for index, row in rdf.iterrows():
        statement="\nMATCH (a"+str(index)+":Artist),(b"+str(index)+""":Artist)
        WHERE a"""+str(index)+".name = '" +row['artist']+ "' AND b"+str(index)+".name = '""" +row['other_artist']+ """'
        CREATE (a"""+str(index)+")<-[r:FEAT]-(b"+str(index)+""")
        WITH a"""+str(index)+",b"+str(index)
        statement = statement[:statement.rfind('\n')] + "\nRETURN r\n"
        db_run_line (statement)
        if index % pbar == 0:
            print(index)

def db_create_artists(adf):
    adf = pd.DataFrame(data=concatenate((adf.artist.values, adf.other_artist.values)), columns=['artist'])
    adf.sort_values(['artist'], inplace=True)
    adf.drop_duplicates(inplace=True)
    adf.reset_index(drop=True, inplace=True)
    adf =df_chunks(adf,1000);
    for df in adf:
        statement=''
        for index,row in df.iterrows():
            statement+="CREATE (:Artist {name:'"+row['artist']+"'})\n"
        # print(statement);
        db_run_line(statement)

def refine_artists(rdf):
    rdf['other_artist'] = rdf['other_artist'].str.lower()
    rdf['artist'] = rdf['artist'].str.lower()
    rdf['other_artist'] = rdf['other_artist'].str.replace(' and ',', ')
    rdf['other_artist'] = rdf['other_artist'].str.replace(' & ',', ')
    virgula = rdf.other_artist.str.contains(',', regex=False)
    virgula.fillna(value=False,inplace=True)
    plusThanOne = rdf[virgula]
    rdf = rdf[~virgula]

    cc = pd.concat ([plusThanOne.artist,plusThanOne.other_artist.str.split(',', expand=True)], axis=1)
    cc = pd.DataFrame(plusThanOne.other_artist.str.split(',').tolist(),index=plusThanOne.artist).stack()
    cc = cc.reset_index([0,'artist'])
    cc.columns = ['artist','other_artist']

    rdf = rdf.append(cc, sort=False)

    rdf['artist']=rdf.artist.str.replace("\\","\\\\")     # Tirando contra-barras
    rdf['artist']=rdf.artist.str.replace("'","\\'")     # Tratando apostrofos simples

    rdf['other_artist']=rdf.other_artist.str.replace('"','')        # Tirando aspas
    rdf['other_artist']=rdf.other_artist.str.replace('radio edit','')        # Tirando termo radio edit
    rdf['other_artist']=rdf.other_artist.str.replace('radio version','')     # Tirando termo radio version
    rdf['other_artist']=rdf.other_artist.str.replace('bonus track','')     # Tirando termo bonus track
    rdf['other_artist']=rdf.other_artist.str.replace("\\","\\\\")     # Tirando contra-barras
    rdf['other_artist']=rdf.other_artist.str.replace("'","\\'")     # Tratando apostrofos simples
    rdf['other_artist']=rdf.other_artist.str.split(' - ').str[0]      # Tirando observacoes pos-hifen
    rdf['other_artist']=rdf.other_artist.str.split('[').str[0]      # Tirando observacoes de colchetes
    rdf['other_artist']=rdf.other_artist.str.split('{').str[0]      # Tirando observacoes de chaves
    rdf['other_artist']=rdf.other_artist.str.split('co-produced by').str[0]      # Tirando co-producao
    rdf['other_artist']=rdf.other_artist.str.split('prod. ').str[0]      # Tirando producao
    rdf['other_artist']=rdf.other_artist.str.split('produced by').str[0]      # Tirando producao
    rdf['other_artist']=rdf['other_artist'].str.strip()             # Tirando espacos no comeco e fim de nomes
    rdf['other_artist']=rdf['other_artist'].str.strip(']')             # Tirando colchetes no comeco e fim de nomes
    rdf['other_artist']=rdf['other_artist'].str.strip('}')             # Tirando chaves no comeco e fim de nomes
    rdf['other_artist']=rdf['other_artist'].str.strip('.')             # Tirando pontos finais ao fim dos artistas
    rdf['other_artist']=rdf.other_artist.replace('','None')         # Nomeando coisas vazias
    rdf['other_artist']=rdf.other_artist.replace(nan,'None')        # Nomeando coisas vazias

    rdf.sort_values(['other_artist'], inplace=True)                 # Ordenando alfabeticamente pelo feat
    rdf = rdf.reset_index(drop=True)                                # Reindexando
    return rdf

def continuous_split(rdf, term):
    redo = rdf[pd.isna(rdf['other_artist'])]
    rdf = rdf[pd.notna(rdf['other_artist'])]

    conn = redo.treated_name.str.split(term, expand=True);
    try:
        redo[['song','other_artist']] = conn[[0,1]].copy(deep=False)
    except KeyError:
        pass;
    rdf = rdf.append(redo, sort=False)
    return rdf

def get_connections(limit=100, maxResults=999999999999):
    r = searchTracks('feat', limit=limit)
    # print (len(r['results']['trackmatches']['track']))     # Qtde de resultados da pagina
    # print (r['results']['opensearch:totalResults'])        # Num total de resultados
    rdf = pd.DataFrame(r['results']['trackmatches']['track'])
    page=1;
    maxResults = min(r['results']['opensearch:totalResults'], maxResults)
    print ('Page:', page, 'Results:', len(r['results']['trackmatches']['track']), 'Total: ',rdf.shape[0], 'maxResults: ', maxResults)

    while (rdf.shape[0]<maxResults and len(r['results']['trackmatches']['track'])>0):
        page+=1
        sleep(0.75);
        r = searchTracks('feat',limit=limit,page=page)
        rdf = rdf.append(pd.DataFrame(r['results']['trackmatches']['track']))
        print ('Page:', page, 'Results:', len(r['results']['trackmatches']['track']), 'Total: ',rdf.shape[0], 'maxResults: ', maxResults)

    rdf = rdf[['name', 'artist']]
    rdf['treated_name'] = rdf['name'].str.lower()
    rdf['treated_name'] = rdf['treated_name'].str.replace('feat.','feat')
    rdf['treated_name'] = rdf['treated_name'].str.replace('(','')
    rdf['treated_name'] = rdf['treated_name'].str.replace(')','')

    rdf = rdf.reset_index(drop=True)
    return (rdf[['artist','treated_name']])

def data_treatment(nres=10000):

    print ("maxResults="+str(nres))

    # Extraindo do
    rdf = get_connections(maxResults=nres)         # Coleta de dados
    rdf.insert(1,'other_artist',None);             # Criacao da coluna other_artist

    # Fazer splitting e gerar os outros artistas conforme varios termos de colaboracao
    terms = ['feat ', 'featring ', 'featuring ', 'feated '];
    for term in terms:
        rdf = continuous_split(rdf.copy(deep=False),term);

    rdf = rdf[pd.notna(rdf['other_artist'])]    # Apos a filtragem, consideramos apenas conexoes que tenham artistas nelas

    rdf.reset_index(drop=True)
    rdf.other_artist= rdf.other_artist.str.title()
    # rdf[['artist','other_artist']].to_csv("connections"+str(nres)+".csv", encoding='utf8', index=False, sep=';')
    rdf = refine_artists(rdf.copy(deep=False))
    # rdf[['artist','other_artist']].to_csv("filtered_connections"+str(rdf.shape[0])+".csv", encoding='utf8', index=False, sep=';')
    db_create_artists(rdf.copy(deep=False))
    db_create_relationships(rdf.copy(deep=False))

try:
    n = int(sys.argv[1]);
except IndexError:
    print ("Entre com o valor")
    sys.exit(1)
except ValueError:
    if sys.argv[1] == 'd':
        print ("Importing default of 10000 artists in 5 sec...");
        sleep(5);
        n=10000;

db_dropall();
data_treatment(n)
