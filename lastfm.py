import requests, json
from time import sleep

API_KEY = '';

# Main function: This function is the main structure that has to be called
# to retrieve each and everything from the API.
def get_lastfm(payload):
    URL = 'http://ws.audioscrobbler.com/2.0/'
    headers = {'user-agent': 'samuels15'}

    payload['api_key']=API_KEY
    payload['format']='json'

    return requests.get(URL, headers=headers, params=payload)

def getTopArtists(limit=500):
    r = get_lastfm({'method':'chart.gettopartists', 'limit':limit});
    # print (r.status_code)
    print (json.dumps(r.json()['artists']['@attr'], sort_keys=True, indent=4));
    return (r.json()['artists'])

def searchTracks(search_term='feat', limit=500, page=1):
    r = get_lastfm({'method':'track.search', 'limit':limit, 'track':search_term, 'page':page});
    # print (json.dumps(r.json()['results']['trackmatches']))
    return r.json()     #['results']

with open('api_key.txt') as f:
    API_KEY = f.read().replace('\n','');

# getTopArtists();
# print (r['opensearch:totalResults'])
