from neo4j import GraphDatabase
import neobolt

def db_run_line(line):
    with open('db_files/sandbox_auth.txt') as f:
        uri = f.readline().replace('\n','')
        user = f.readline().replace('\n','')
        pwd = f.readline().replace('\n','')
    try:
        driver = GraphDatabase.driver(uri, auth=(user, pwd), encrypted=False)
        with driver.session() as session:
            session.run(line)
    except neobolt.exceptions.ServiceUnavailable:
        print ("Error")
        print (line)
        pass;

def db_test_connection():
    with open('db_files/sandbox_auth.txt') as f:
        uri = f.readline().replace('\n','')
        user = f.readline().replace('\n','')
        pwd = f.readline().replace('\n','')

    try:
        driver = GraphDatabase.driver(uri, auth=(user, pwd))
    except neobolt.exceptions.ServiceUnavailable:
        print ("Timeout.")
        return;
    print ("Success")
    return;
